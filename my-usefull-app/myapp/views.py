# myapp/views/py
from myapp import myapp, mydb
from flask import render_template, redirect, flash, url_for
from myapp.forms import SubscribeForm
from myapp.models import User

# set content head and body static
# later we can get this from a database
content_head = "Lorem Ipsum"
content_body = "bla erwin testins123 Where does it come from? " \
               "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of " \
               "classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin " \
               "professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, " \
               "consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, " \
               "discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus " \
               "Bonorum et Malorum' (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise " \
               "on the theory of ethics, very popular during the Renaissance. " \
               "The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', comes from a line in section 1.10.32." \
               "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested." \
                "Sections 1.10.32 and 1.10.33 from 'de Finibus Bonorum et Malorum' by Cicero are also reproduced in " \
               "their exact original form, accompanied by English versions from the 1914 translation by H. Rackham."

@myapp.route("/")
def main_index():
    return render_template("welcome.html", content_head=content_head, content_body=content_body)


@myapp.route("/webinars")
def webinar_main():
    return render_template("webinars.html")


@myapp.route("/newsletter", methods=["POST", "GET"])
def newsletter_main():
    form = SubscribeForm()
    if form.validate_on_submit():
        flash('{} subscribed'.format(form.name.data))
        new_subscriber = User(name=form.name.data, eventname="")
        mydb.session.add(new_subscriber)
        mydb.session.commit()
        mydb.session.close()

        return redirect(url_for('newsletter_main'))
    return render_template("newsletter.html", form=form)