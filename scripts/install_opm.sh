#!/usr/bin/env bash
# source: https://docs.openshift.com/container-platform/4.6/cli_reference/opm-cli.html#opm-cli


# Set the REG_CREDS environment variable to the file
# path of your registry credentials for use in later steps.
REG_CREDS=${XDG_RUNTIME_DIR}/containers/auth.json

# Authenticate with registry.redhat.io
sudo podman login registry.redhat.io

# Extract the opm binary for the operating system you want from the
# Operator Registry image and copy it to your local file system.
# for Linux
oc image extract registry.redhat.io/openshift4/ose-operator-registry:v4.6 \
    -a ${REG_CREDS} \
    --path /usr/bin/opm:. \
    --confirm

# for macOS
#oc image extract registry.redhat.io/openshift4/ose-operator-registry:v4.6 \
#    -a ${REG_CREDS} \
#    --path /usr/bin/darwin-amd64-opm:. \
#    --confirm
#Rename the file to opm:
# mv darwin-amd64-opm opm

#For Linux or macOS, make the binary executable
chmod +x ./opm

# Place the file anywhere in your PATH.
sudo mv ./opm /usr/local/bin/

# Verify that the client was installed correctly
opm version
